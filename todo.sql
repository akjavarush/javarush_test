CREATE TABLE test_todo(
    id INT NOT NULL auto_increment, 
    title TEXT NOT NULL,
    adding_date DATE NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO test_todo(title, adding_date)
values
("������� GIT", now()),
("��������� ������������ �� Spring", now()),
("����������� � REST API", now()),
("��������� ����������� Spring MVC �����������", now()),
("�������� � Spring MVC ��������� ���� MySQL", now()),
("����������� ��� ��� ������ Hibernate �������� ������ �� ��", now()),
("�������� � view ���������� �������� AngularJS", now()),
("������� $http ������� � �������� � AngularJS", now()),
("���������� ���������� ���, ��� �� ������ ���������� �� �� View, � ����� json ��������", now()),
("������� ����������� �������������� ���������� �� ������ AngularJS", now()),
("���������� ������ ������ ToDo � ���������� � �������", now()),
("������������ � AngularJS � ������� ��������������� ��� ���������� �� ���", now()),
("����������� �� AngularJS paging ����� � ������������ ������ ���������� ����� �� ��������", now()),
("����������� �� AngularJS ����� ������ �� �����", now()),
("��������� ������ �������� ����", now()),
("���������� ���", now()),
("������� ������� �� JavaRush", now());