/**
 * Created by propant on 08.02.2016.
 */
'use strict';

App.filter('pagination', function() {
    return function(input, start) {
        start = parseInt(start, 10);
        return input.slice(start);
    };
});

App.controller('TaskController', ['$scope', 'TaskService','filterFilter', function($scope, TaskService, filterFilter) {
    var self = this;
    self.task={id:null, title:'', addingDate:'', done:''};
    self.tasks=[];

    self.listAll = 'inactive';
    self.listDone = 'inactive';
    self.listNotDone = 'active';

    self.pageSizeOptions = [5,10,15,20];

/****************** Pagination ***********************/


    self.search = {};


    self.currentPage = 0;

    self.filteredItems = [];
    self.itemsPerPage = 5;
    self.pagedItems = [];

    // filter the items following the search string
    self.search = function () {
       self.filteredItems = filterFilter(self.tasks,$scope.query);

        self.currentPage = 0;
        self.groupToPages();
    };

    self.groupToPages = function () {
        self.pagedItems = [];

        for (var i = 0; i < self.filteredItems.length; i++) {
            if (i % self.itemsPerPage === 0) {
                self.pagedItems[Math.floor(i / self.itemsPerPage)] = [ self.filteredItems[i] ];
            } else {
                self.pagedItems[Math.floor(i / self.itemsPerPage)].push(self.filteredItems[i]);
            }
        }
    };

    self.range = function (start, end) {
        var ret = [];
        if (!end) {
            end = start;
            start = 0;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }
        return ret;
    };

    self.prevPage = function () {
        if (self.currentPage > 0) {
            self.currentPage--;
        }
    };

    self.nextPage = function () {
        if (self.currentPage < self.pagedItems.length - 1) {
            self.currentPage++;
        }
    };


    self.setPage = function (n) {
        self.currentPage = n;
    };

   ///////////// Pagination /////////////////////////


    self.fetchAllTasks = function(){
        TaskService.fetchAllTasks()
            .then(
                function(d) {
                    self.tasks = d;

                    self.listAll = 'active';
                    self.listDone = 'inactive';
                    self.listNotDone = 'inactive';
                    self.search();


                },
                function(errResponse){
                    console.error('Error while fetching all TASKS');
                }
            );
    };

    self.fetchDoneTasks = function(){
        TaskService.fetchDoneTasks()
            .then(
                function(d) {
                    self.tasks = d;

                    self.listAll = 'inactive';
                    self.listDone = 'active';
                    self.listNotDone = 'inactive';
                    self.search();

                },
                function(errResponse){
                    console.error('Error while fetching all TASKS');
                }
            );
    };

    self.fetchNotDoneTasks = function(){
        TaskService.fetchNotDoneTasks()
            .then(
                function(d) {
                    self.tasks = d;

                    self.listAll = 'inactive';
                    self.listDone = 'inactive';
                    self.listNotDone = 'active';
                    self.search();
                },
                function(errResponse){
                    console.error('Error while fetching all TASKS');
                }
            );
    };

    self.fetchTasks = function()
    {

        if (self.listAll === 'active'){

            self.fetchAllTasks();
        } else if (self.listDone === 'active')
        {
            self.fetchDoneTasks();
        } else {
            self.fetchNotDoneTasks();
        }

    }

    self.createTask = function(task){
        TaskService.createTask(task)
            .then(
                self.fetchTasks,
                function(errResponse){
                    console.error('Error while creating Task.');
                }
            );
        self.reset();
    };

    self.updateTask = function(task, id){
        TaskService.updateTask(task, id)
            .then(
                self.fetchTasks,
                function(errResponse){
                    console.error('Error while updating Task.');
                }
            );
    };

    self.deleteTask = function(id){
        TaskService.deleteTask(id)
            .then(
                self.fetchTasks,
                function(errResponse){
                    console.error('Error while deleting Task.');
                }
            );
    };

    self.markdone = function(id){
        console.log('Task id ', id, ' will be marked as DONE');
        TaskService.markTaskAsDone(id)
            .then(
                    self.fetchTasks,
                function(errResponse){
                    console.error('Error while marking Task as DONE.');
                }
            );
    }

    self.marknotdone = function(id){
        console.log('Task id ', id, ' will be marked as DONE');
        TaskService.markTaskAsNotDone(id)
            .then(
                    self.fetchTasks,
                function(errResponse){
                    console.error('Error while marking Task as NOT DONE.');
                }
            );
    }


    self.fetchTasks();


    self.submit = function() {
        if(self.task.id === null){
            console.log('Saving New Task', self.task);
            self.createTask(self.task);
        }else{
            self.updateTask(self.task, self.task.id);
            console.log('Task updated with id ', self.task.id);
        }
        self.reset();
    };

    self.edit = function(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.tasks.length; i++){
            if(self.tasks[i].id === id) {
                self.task = angular.copy(self.tasks[i]);
                break;
            }
        }
    };



    self.remove = function(id){
        console.log('id to be deleted', id);

        if(self.task.id === id) {

            console.log('cleans the form', id);
            self.reset();
        }
        console.log('deletes the task', id);
        self.deleteTask(id);
    };


    self.reset = function(){
        self.task = { id:null, title:'', addingDate:'', done:'' };
        console.log("Clearing the form");
        $scope.myForm.$setPristine();
        console.log("The form is cleared");
    };
}]);
