'use strict';

App.factory('TaskService', ['$http', '$q', function($http, $q){

    return {

        fetchAllTasks: function() {
            return $http.get('list')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching ALL tasks');
                        return $q.reject(errResponse);
                    }
                );
        },

        fetchDoneTasks: function(){
            return $http.get('allDone')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching all DONE tasks');
                        return $q.reject(errResponse);
                    }
                );
        },

        fetchNotDoneTasks: function(){
            return $http.get('allNotDone')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching all Not done tasks');
                        return $q.reject(errResponse);
                    }
                );
        },

        createTask: function(task){
            return $http.post('new', task)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while CREATING new Task' + task);
                        return $q.reject(errResponse);
                    }
                );
        },

        updateTask: function(task, id){
            return $http.post('update-task-'+id, task)
                .then(
                    function(response)
                    {
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while UPDATING task ' + id);
                        return $q.reject(errResponse);
                    }
                );
        },

        deleteTask: function(id){
            return $http.delete('delete-task-'+id)
                . then(
                    function(response)
                    {
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while DELETING task ' + id);
                        return $q.reject(errResponse);
                    }
                );
        },

        markTaskAsDone: function(id){
            return $http.put('done-task-'+id)
                . then(
                    function(response)
                    {
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while MARKING task AS DONE ' + id);
                        return $q.reject(errResponse);
                    }
                );
        },
        markTaskAsNotDone: function(id){
            return $http.put('undone-task-'+id)
                . then(
                    function(response)
                    {
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while MARKING task AS NOT UNdonde ' + id);
                        return $q.reject(errResponse);
                    }
                );
        }

    };

}]);