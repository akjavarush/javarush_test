<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>TODO List: JavaRush Test</title>
    <link rel="stylesheet" href="<c:url value='/static/css/foundation.min.css' />" rel="stylesheet">
    <link href="<c:url value='/static/css/1.css' />" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body ng-app="myApp" class="ng-cloak">
<div class="main-container" ng-controller="TaskController as ctrl">
    <div class="row column">
        <div class="small-12 medium-6 columns">
            <h2>Add new Task</h2>

            <div class="row columns">
                <form ng-submit="ctrl.submit()" name="myForm" novalidate>
                    <input type="hidden" ng-model="ctrl.task.id" />
                    <input type="hidden" ng-model="ctrl.task.addingDate" />
                    <div>
                        <span class="input-group-label">Task</span>
                        <textarea type="text" ng-model="ctrl.task.title" id="taskInputField" placeholder="I need to Do" required ng-minlength="1"/></textarea>
                    </div>
                    <div class="input-group-button">
                        <input type="submit"  value="{{!ctrl.task.id ? 'Add' : 'Update'}}" class="button full" ng-disabled="myForm.$invalid">

                    </div>
                </form>
            </div>
        </div>
        <div class="small-12 medium-6 columns">
            <h2>List of ToDos</h2>
            <div class="row">
                <div class="button-group filter tiny small-12 medium-6 columns">
                    <a class="button" ng-class="ctrl.listNotDone" ng-click="ctrl.fetchNotDoneTasks()">Not Done</a>
                    <a class="button" ng-class="ctrl.listAll" ng-click="ctrl.fetchAllTasks()">All</a>
                    <a class="button" ng-class="ctrl.listDone" ng-click="ctrl.fetchDoneTasks()">Done</a>
                </div>
                <div class="small-12 medium-6 columns">
                   <select class="right" ng-model="ctrl.itemsPerPage" ng-options="item for item in ctrl.pageSizeOptions" ng-change="ctrl.fetchTasks()" ></select>
                </div>
            </div>


            <div class="input-group search">
                <input class="input-group-field" type="text" ng-model="query" ng-change="ctrl.search()" placeholder="Search">
                <div class="input-group-button">
                    <input type="submit" class="button" value="Search">
                </div>
            </div>
            <table class="hover">
                <tbody>
                <tr ng-repeat="u in ctrl.pagedItems[ctrl.currentPage]">

                    <td ng-class="{markDone: u.done}">
                        <p ng-bind="u.title"></p>
                        <p class="tiny button-group">
                            <button type="button" ng-click="ctrl.edit(u.id)" class="alarm button">Edit</button>
                            <button type="button" ng-click="ctrl.remove(u.id)" class="secondary button">Remove</button>
                        </p>
                    </td>
                    <td class="tiny button-group">
                        <button type="button" ng-if="u.done" ng-click="ctrl.marknotdone(u.id)" class="secondary button">Undone</button>
                        <button type="button" ng-if="u.done == false" ng-click="ctrl.markdone(u.id)" class="success button">Done</button></td>

                </tr>
            </table>

            <ul class="pagination text-center" role="navigation" aria-label="Pagination">
                <li class="pagination-previous">
                    <a href="" ng-class="{disabled: ctrl.currentPage == 0}" ng-click="ctrl.prevPage()">Previous</a>
                </li>
                <li ng-repeat="n in ctrl.range(ctrl.pagedItems.length)" ng-class="{current: n == ctrl.currentPage}" ng-click="ctrl.setPage(n)">
                    <a href="" ng-bind="n + 1">1</a>
                </li>
                <li class="pagination-next">
                    <a href="" ng-class="{disabled : ctrl.currentPage == ctrl.pagedItems.length-1}" ng-click="ctrl.nextPage()">Next</a>
                </li>
            </ul>
         </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
<script src="<c:url value='/static/js/app.js' />"></script>
<script src="<c:url value='/static/js/service/todo_service.js' />"></script>
<script src="<c:url value='/static/js/controller/todo_controller.js' />"></script>

</body>
</html>