package com.propant.jr.service;

import com.propant.jr.model.Task;

import java.util.List;

/**
 * Created by propant on 03.02.2016.
 */
public interface TodoService {

    Task findById(int id);

    void saveTask(Task task);

    void updateTask(Task task);

    void deleteTaskById(String id);
    void markTaskAsDone(String id);
    void markTaskAsNotDone(String id);

    List<Task> findAllDoneTasks();
    List<Task> findAllNotDoneTasks();

    List<Task> findAllTasks();

}
