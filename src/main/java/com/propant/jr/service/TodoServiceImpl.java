package com.propant.jr.service;

import com.propant.jr.dao.TodoDao;
import com.propant.jr.model.Task;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by propant on 03.02.2016.
 */

@Service("TodoService")
@Transactional
public class TodoServiceImpl implements TodoService {

    @Autowired
    private TodoDao dao;

    public Task findById(int id) {
        return dao.findById(id);
    }

    public void saveTask(Task task) {

        if (task.getAddingDate() == null)
            task.setAddingDate(new LocalDate());

        dao.saveTask(task);
    }


    public void updateTask(Task task) {
        Task entity = dao.findById(task.getId());

        if (entity != null)
        {
            entity.setTitle(task.getTitle());
            entity.setAddingDate(task.getAddingDate());
        }
    }

    public void deleteTaskById(String id) {
        dao.deleteTaskById(id);
    }

    public void markTaskAsDone(String id) {
        dao.markTaskAsDone(id);
    }

    public void markTaskAsNotDone(String id) {
        dao.markTaskNotDone(id);
    }

    public List<Task> findAllDoneTasks() {
        return dao.findAllDoneTasks();
    }

    public List<Task> findAllNotDoneTasks() {
        return dao.findAllNotDoneTasks();
    }

    public List<Task> findAllTasks() {
        return dao.findAllTasks();
    }
}
