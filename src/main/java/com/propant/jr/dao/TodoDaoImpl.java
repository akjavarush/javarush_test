package com.propant.jr.dao;

import com.propant.jr.model.Task;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by propant on 03.02.2016.
 */

@Repository("TodoDao")
public class TodoDaoImpl extends AbstractDao<Integer, Task> implements TodoDao  {

    public Task findById(int id) {
        return getByKey(id);
    }

    public void saveTask(Task task) {

        persist(task);
    }

    public void deleteTaskById(String id) {
        Query query = getSession().createSQLQuery("delete from Test_todo where id = :id");
        query.setString("id", id);
        query.executeUpdate();
    }

    public void markTaskAsDone(String id) {
        Query query = getSession().createSQLQuery("update Test_todo set done = 1  WHERE id = :id").addEntity(Task.class);
        query.setString("id", id);
        query.executeUpdate();
    }

    public void markTaskNotDone(String id) {
        Query query = getSession().createSQLQuery("update Test_todo set done = 0  WHERE id = :id").addEntity(Task.class);
        query.setString("id", id);
        query.executeUpdate();
    }


    @SuppressWarnings("unchecked")
    public List<Task> findAllTasks() {

        Query query = getSession().createSQLQuery("select * from Test_todo order by adding_date DESC, title ASC").addEntity(Task.class);
        List<Task> all = query.list();

        return all;
    }

    @SuppressWarnings("unchecked")
    public List<Task> findAllDoneTasks() {
        Query query = getSession().createSQLQuery("select * from Test_todo WHERE done = 1 order by adding_date ASC, title ASC").addEntity(Task.class);
        List<Task> allDone = query.list();

        return allDone;
    }

    @SuppressWarnings("unchecked")
    public List<Task> findAllNotDoneTasks() {
        Query query = getSession().createSQLQuery("select * from Test_todo WHERE done = 0 order by adding_date ASC, title ASC").addEntity(Task.class);
        List<Task> allDone = query.list();

        return allDone;
    }

}
