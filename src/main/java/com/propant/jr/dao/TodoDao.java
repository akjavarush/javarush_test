package com.propant.jr.dao;

import com.propant.jr.model.Task;

import java.util.List;

/**
 * Created by propant on 03.02.2016.
 */
public interface TodoDao {

    Task findById(int id);

    void saveTask(Task task);
    void deleteTaskById(String id);
    void markTaskAsDone(String id);
    void markTaskNotDone(String id);


    List<Task> findAllTasks();
    List<Task> findAllDoneTasks();
    List<Task> findAllNotDoneTasks();
}
