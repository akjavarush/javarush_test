package com.propant.jr.controller;

import com.propant.jr.model.Task;
import com.propant.jr.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * Created by propant on 08.02.2016.
 */

@RestController

public class TodoRestController {

    @Autowired
    TodoService service;

    @Autowired
    MessageSource messageSource;

    @RequestMapping(value = { "/", "/list" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
    public ResponseEntity<List<Task>> listTasks() {

        List<Task> tasks = service.findAllTasks();
        if(tasks.isEmpty()){
            return new ResponseEntity<List<Task>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }

        return new ResponseEntity<List<Task>>(tasks, HttpStatus.OK);

    }

    @RequestMapping(value = { "/allDone" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
    public ResponseEntity<List<Task>> listDoneTasks() {

        List<Task> tasks = service.findAllDoneTasks();
        if(tasks.isEmpty()){
            return new ResponseEntity<List<Task>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }

        return new ResponseEntity<List<Task>>(tasks, HttpStatus.OK);

    }

    @RequestMapping(value = { "/allNotDone" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
    public ResponseEntity<List<Task>> listNotDoneTasks() {

        List<Task> tasks = service.findAllNotDoneTasks();
        if(tasks.isEmpty()){
            return new ResponseEntity<List<Task>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }

        return new ResponseEntity<List<Task>>(tasks, HttpStatus.OK);

    }


    @RequestMapping(value = { "/task-{id}" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<Task> editTask(@PathVariable String id ) {

        System.out.println("Fetching User with id " + id);
        Task task = service.findById(Integer.valueOf(id));
        if (task == null) {
            System.out.println("Task with id " + id + " not found");
            return new ResponseEntity<Task>(HttpStatus.NOT_FOUND);
        }
        System.out.println("Task with id " + id + " is found");
        return new ResponseEntity<Task>(task, HttpStatus.OK);


    }

    //-------------------Create Task --------------------------------------------------------

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public ResponseEntity<Void> saveTask(@RequestBody Task task) {
        System.out.println("Creating Task " + task.getTitle());

        task.setDone(false);
        service.saveTask(task);

        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }




    // -------------------------- Update Task -------------------------------------

    @RequestMapping(value = {"/update-task-{id}"}, method = RequestMethod.POST)
    public ResponseEntity<Task> updateTask(@PathVariable String id, @RequestBody Task task) {
        System.out.println("Updating Task " + id);


        service.updateTask(task);

        return new ResponseEntity<Task>(task, HttpStatus.OK);
    }

    //------------------- Delete a Task --------------------------------------------------------


    @RequestMapping(value = { "/delete-task-{id}" }, method = RequestMethod.DELETE)
    public ResponseEntity<Task> deleteTask(@PathVariable String id) {
        service.deleteTaskById(id);
        return new ResponseEntity<Task>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = { "/done-task-{id}" }, method = RequestMethod.PUT)
    public ResponseEntity<Task> doneTask(@PathVariable String id) {
        service.markTaskAsDone(id);
        return new ResponseEntity<Task>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = { "/undone-task-{id}" }, method = RequestMethod.PUT)
    public ResponseEntity<Task> unDoneTask(@PathVariable String id) {
        service.markTaskAsNotDone(id);
        return new ResponseEntity<Task>(HttpStatus.NO_CONTENT);
    }
}
